FROM alpine:3.16

LABEL org.opencontainers.image.authors = "cusoon"
LABEL org.opencontainers.image.source = "https://gitlab.com/homesrvr/docker-radarr"

WORKDIR /app/radarr/bin
SHELL ["/bin/ash", "-o", "pipefail", "-c"]
RUN \
  apk add --no-cache curl jq libintl sqlite-libs icu-libs libmediainfo xmlstarlet && \
  mkdir -p /config /movies /downloads /app/radarr/bin && \
  RADARR_VERSION=$(curl -sX GET 'https://api.github.com/repos/radarr/radarr/releases/latest' | jq -r .tag_name) && \
  RADARR_VERSION_SHORT="${RADARR_VERSION:1}" && \
  export RADARR_VERSION && \
  curl -s -o /tmp/radarr.tar.gz -L "https://github.com/Radarr/Radarr/releases/download/${RADARR_VERSION}/Radarr.master.$RADARR_VERSION_SHORT.linux-musl-core-x64.tar.gz" && \
  tar xzf /tmp/radarr.tar.gz -C /app/radarr/bin --strip-components=1 && \
  rm -rf "$HOME/.cache" "/var/cache/apk" "/app/radarr/bin/Radarr.Update" "/tmp/*" "/var/tmp/*"

COPY /startup.sh /app/radarr/bin/startup.sh

HEALTHCHECK --start-period=1m --timeout=5s --interval=1m \
  CMD curl -fsSL "http://localhost:7878/" || exit 1

# ports and volumes
EXPOSE 7878
VOLUME /config /movies /downloads
ENTRYPOINT ["sh", "/app/radarr/bin/startup.sh"]


# Container labels
ARG BUILD_DATE
ARG BUILD_REF
LABEL org.opencontainers.image.authors = "cusoon"
LABEL org.opencontainers.image.source = "https://gitlab.com/homesrvr/docker-radarr"
LABEL org.label-schema.build-date=${BUILD_DATE}
LABEL org.label-schema.vcs-ref=${BUILD_REF} 
LABEL org.label-schema.name="radarr"
LABEL org.label-schema.schema-version="$RADARR_VERSION"
